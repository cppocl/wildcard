/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType, typename SizeType = std::size_t>
bool WildCards<CharType, SizeType>::UnsafeMatch(char_type const* text,
                                                size_type text_length,
                                                char_type const* wild_cards) noexcept
{
    typedef PartialWildCards<CharType, SizeType> partial_wild_cards_type;

    bool matched = false;
    char_type const* remaining_wild_cards = wild_cards;

    while (remaining_wild_cards != nullptr && !matched)
        remaining_wild_cards = PartialWildCards<CharType, SizeType>::UnsafePartialMatch(
            text, text_length, remaining_wild_cards, matched);

    return matched;
}

template<typename CharType, typename SizeType = std::size_t>
bool WildCards<CharType, SizeType>::Match(char_type const* text,
                                          size_type text_length,
                                          char_type const* wild_cards) noexcept
{
    return text != nullptr && wild_cards != nullptr
        ? UnsafeMatch(text, text_length, wild_cards)
        : false;
}
