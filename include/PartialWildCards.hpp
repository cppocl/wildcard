/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_WILDCARD_INCLUDE_PARTIALWILDCARDS_HPP
#define OCL_GUARD_WILDCARD_INCLUDE_PARTIALWILDCARDS_HPP

#include "../../char/CharConstants.hpp"

namespace ocl
{

template<typename CharType, typename SizeType = std::size_t>
class PartialWildCards
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;

    static char_type const NullChar = CharConstants<char_type>::Null;
    static char_type const BackSlashChar = CharConstants<char_type>::BackSlash;
    static char_type const ForwardSlashChar = CharConstants<char_type>::ForwardSlash;
    static char_type const FullStopChar = CharConstants<char_type>::FullStop;
    static char_type const QuestionMarkChar = CharConstants<char_type>::QuestionMark;
    static char_type const AsteriskChar = CharConstants<char_type>::Asterisk;
    static char_type const PercentChar = CharConstants<char_type>::Percent;
    static char_type const SemiColonChar = CharConstants<char_type>::SemiColon;
    static char_type const VerticalBar = CharConstants<char_type>::VerticalBar;

    static size_type const SIZE_TYPE_MAX = ~static_cast<size_type>(0);

public:
    /// Find match against partial wild card, e.g. ignore additional wild cards following ';'.
    /// It is assumed the text is not null and has at least 1 character.
    /// return next wild card or null if there are no more wild cards.
    static char_type const* UnsafePartialMatch(char_type const* text,
                                               size_type text_length,
                                               char_type const* wild_cards,
                                               bool& matched) noexcept
    {
        matched = false;
        size_type pos = text_length;

        // Minimum and maximum expected range for number of characters in the text.
        size_type wild_chars_min = 0;
        size_type wild_chars_max = SIZE_TYPE_MAX;

        // Loop the wild card, looking for anything that is not *, % or ?.
        char_type wild_card_char;
        while (wild_cards != nullptr)
        {
            wild_card_char = *wild_cards;
            switch (wild_card_char)
            {
            case NullChar:
                wild_cards = nullptr;
                /* no break */

            case SemiColonChar:
                // Reached ';' or '\0' without finding a problem, so must be a match.
                matched = wild_chars_max >= text_length;
                return wild_cards;

            case AsteriskChar:
            case PercentChar:
            case QuestionMarkChar:
                wild_cards = GetWildCardCount(wild_cards, wild_chars_min, wild_chars_max);

                // When the minimum characters expected is greater than the length of the text,
                // it is not possible to have a match.
                if (wild_chars_min > text_length)
                    return nullptr;
                break;

                // Any non-matching characters are handled here.
            default:
                // Match the wildcard character from wild_cards that are not * % and ?.
                pos = UnsafeGetCharPos(text, wild_card_char);

                // pos reflects the first character that is not * % or ?.
                // This should be not be before the minimum number of wild card characters.
                if (pos > text_length || pos < wild_chars_min)
                    return nullptr;
                text += pos + 1;
                text_length -= pos + 1;
                ++wild_cards;

                // Once the * % and ? wild cards have been skipped, any following characters
                // will start from position 0.
                wild_chars_min = 0;
            }
        }

        return wild_cards;
    }

private:
    // Count minimum and maximum number of characters expected for * % and ?
    // Return null if there are no more characters to be parsed in wild_cards.
    // Always return first character after * % or ?.
    // NOTE: For * and % wild_chars_max will = maximum integer size.
    static char_type const* GetWildCardCount(char_type const* wild_cards,
                                             size_type& wild_chars_min,
                                             size_type& wild_chars_max) noexcept
    {
        // Count *, % and ? characters for comparing to any characters in the text.

        wild_chars_min = wild_chars_max = 0;

        while (true)
        {
            switch (*wild_cards)
            {
            case PercentChar:
                ++wild_chars_min;
                /* no break */

            case AsteriskChar:
                wild_chars_max = SIZE_TYPE_MAX;
                break;

            case QuestionMarkChar:
                ++wild_chars_min;
                if (wild_chars_max < SIZE_TYPE_MAX)
                    ++wild_chars_max;
                break;

            default:
                // Not a wild card, so nothing to count.
                return wild_cards;
            }
            ++wild_cards;
        }
    }

    // Get the matching character position from the start,
    // or return the position of '\0'.
    static size_type UnsafeGetCharPos(char_type const* s, char_type c) noexcept
    {
        char_type const* end = s;
        while (*end != c && *end != NullChar)
            ++end;
        return static_cast<size_type>(end - s);
    }
};

} // namespace ocl

#endif // OCL_GUARD_WILDCARD_INCLUDE_PARTIALWILDCARDS_HPP
