/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../WildCards.hpp"
#include <string>

TEST_MEMBER_FUNCTION(WildCard, Match, NA)
{
    typedef ocl::WildCards<char> wild_cards_type;

    std::string filename = "a.txt";

    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "*"));
    //CHECK_TRUE([=]() {return std::regex_match(filename.c_str(), std::basic_regex<char>(".*")); });

    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a*"));
    //CHECK_TRUE([=]() {return std::regex_match(filename.c_str(), std::basic_regex<char>("a.*")); });

    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "*a"));
    //CHECK_TRUE([=]() {return std::regex_match(filename.c_str(), std::basic_regex<char>(".*a.*")); });

    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a*t"));
    //CHECK_TRUE([=]() {return std::regex_match(filename.c_str(), std::basic_regex<char>(".*a.*t")); });

    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a*t*"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a*tx*?"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a*tx*"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a*tx**"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "%"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a%"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a%t"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a%t%"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a%tx%"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a%tx?"));
    CHECK_TRUE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a.txt"));

    CHECK_FALSE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "?a"));
    CHECK_FALSE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "%a"));
    CHECK_FALSE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "a%tx??"));
    CHECK_FALSE(wild_cards_type::UnsafeMatch(filename.c_str(), filename.length(), "????"));
}
