# C++ Wildcard

![](header_image.jpg)

## Overview

Wildcard text parsing library, which can be used for file finding or text searching within documents.  
  
This is designed to be an easier to use alternative to RegEx, but use the same syntax for matching filenames and text within documented.

## Syntax

```
*    0 or more of any character.  
%    1 or more of any character.  
?    any 1 character.  
```

## Follow

Telegram messenger: https://t.me/cppocl

## Code Examples

```cpp
#include "wildcard/WildCards.hpp"
#include <cstring>
#include <cassert>

int main()
{
    char const* filename = "a.txt";
    size_t length = strlen(filename);

    bool matched = wild_cards_type::Match(filename, length, "*");
    assert(matched);

    matched = wild_cards_type::Match(filename, length, "a*");
    assert(matched);

    matched = wild_cards_type::Match(filename, length, "a%tx?")
    assert(matched);
}
```
