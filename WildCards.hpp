/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOOLS_INCLUDE_FILE_WILDCARDS_HPP
#define OCL_GUARD_TOOLS_INCLUDE_FILE_WILDCARDS_HPP

#include "include/PartialWildCards.hpp"
#include <cstddef>
#include <cstring>

namespace ocl
{

/// Handle a common matching algorithm for texts, which behaves the same on all platforms.
/// Following characters have special meaning for matching:
///   *   Match all characters fro 0 to all characters in the wild card,
///       until all characters are matched, or following characters are discovered in the wild card.
///   %   Match all characters fro 1 to all characters in the wild card,
///       until all characters are matched, or following characters are discovered in the wild card.
///   ?   Match any single character.
template<typename CharType, typename SizeType = std::size_t>
class WildCards
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;

    static char_type const NullChar         = CharConstants<char_type>::Null;
    static char_type const BackSlashChar    = CharConstants<char_type>::BackSlash;
    static char_type const ForwardSlashChar = CharConstants<char_type>::ForwardSlash;
    static char_type const FullStopChar     = CharConstants<char_type>::FullStop;
    static char_type const QuestionMarkChar = CharConstants<char_type>::QuestionMark;
    static char_type const AsteriskChar     = CharConstants<char_type>::Asterisk;
    static char_type const PercentChar      = CharConstants<char_type>::Percent;
    static char_type const SemiColonChar    = CharConstants<char_type>::SemiColon;
    static char_type const VerticalBar      = CharConstants<char_type>::VerticalBar;

    static size_type const SIZE_TYPE_MAX    = ~static_cast<size_type>(0);

public:
    /// Match a text to a semi-colon separated list of wild cards.
    /// E.g. "*", "*.*", "*.txt;*.
    /// NOTE: text and wild_cards must not be null.
    static bool UnsafeMatch(char_type const* text,
                            size_type text_length,
                            char_type const* wild_cards) noexcept;

    static bool Match(char_type const* text,
                      size_type text_length,
                      char_type const* wild_cards) noexcept;
};

#include "include/WildCards.inl"

} // namespace ocl

#endif // OCL_GUARD_TOOLS_INCLUDE_FILE_WILDCARDS_HPP
